import React, { Component } from 'react';
import logo from './logo.svg';
import LinkForm from './components/LinkForm'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">

        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>URL Shortner</h2>
        </div>
        <LinkForm />
      </div>
    );
  }
}

export default App;
