import React from 'react';
import ClipboardButton from 'react-clipboard.js';

class LinkForm extends React.Component {
  constructor(props) {
    super(props);
    this.apiHost = process.env.REACT_APP_API_HOST
    this.state = {url: '', shortURL: '', copyText: 'Copy'};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.copiedText = this.copiedText.bind(this);

  }

  handleChange(event) {
    this.setState({url: event.target.value});
  }

  handleSubmit(event) {
    fetch(this.apiHost, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ url: this.state.url })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({shortURL: responseJson.short_url});
    })
    event.preventDefault();
  }

  copiedText() {
    this.setState({copyText: "Copied!"})
  }

  render() {
    if (this.state.shortURL) {
      var absoluteURL = this.apiHost + this.state.shortURL
      var shortURL = <div className="shortenedURL">
        Woo! Here is your short URL: <a id="url" href={absoluteURL}>{absoluteURL}</a>
        <p className="copy">
          <ClipboardButton data-clipboard-text={absoluteURL} onSuccess={this.copiedText}>
            {this.state.copyText}
          </ClipboardButton>
        </p>
      </div>
    }
    return (
      <div>
        <form className="linkForm" onSubmit={this.handleSubmit}>
          <label>
            <input type="text" value={this.state.url} onChange={this.handleChange} placeholder="Enter URL..." />
          </label>
        </form>
        {shortURL}
      </div>
    );
  }
}

export default LinkForm;
