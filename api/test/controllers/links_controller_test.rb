require 'test_helper'

class LinksControllerTest < ActionDispatch::IntegrationTest
  test "can create a new link" do
    post "/", params: { url: "http://www.google.com" }
    assert_response :success
  end

  test "should get the same shortcode even if the protocol is missing" do
    post "/", params: { url: "http://www.google.com" }
    assert_response :success

    link = Link.last
    expected = {
      "short_url"   => link.short_url,
      "short_code"  => link.short_code,
      "url"         => link.url
    }

    post "/", params: { url: "www.google.com" }
    assert_response :success
    assert_equal(expected, response.parsed_body)
  end

  test "can redirect to short url" do
    link = Link.create(url: "http://www.farmdrop.com")
    get link_url(short_code: link.short_code)
    assert_redirected_to "http://www.farmdrop.com"
  end
end
