require 'test_helper'

class LinkTest < ActiveSupport::TestCase

  test "should not save link without url" do
    link = Link.new
    assert_not link.save
  end

  test "should not save link with blank url" do
    link = Link.new(url: " ")
    assert_not link.save
  end

  test "should have a unique url" do
    2.times { Link.create(url: "http://www.farmdrop.com") }
    assert_equal Link.where(url: "http://www.farmdrop.com").size, 1
  end

  test "should have get a unique short_code" do
    link = Link.create(url: "http://www.farmdrop.com")
    assert_not_nil link.short_code, "Saved link without a short_code"
  end
end
