require 'test_helper'

class UrlProtocolFixerTest < ActiveSupport::TestCase
  test "should handle urls without a http:// protocol" do
    link = UrlProtocolFixer.new(url: "farmdrop.com")
    assert_equal link.url, "http://farmdrop.com"
  end
end
