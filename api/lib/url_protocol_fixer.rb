class UrlProtocolFixer
  def initialize(url:)
    @url = url
  end

  def url
    return "http://#{@url}" unless @url.blank? || @url.include?('http')
    @url
  end
end
