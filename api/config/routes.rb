Rails.application.routes.draw do
  resources :links, only: [:create, :show], param: :short_code, path: "/"

end
