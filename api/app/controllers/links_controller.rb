class LinksController < ApplicationController
  include ActionController::MimeResponds

  def create
    url = UrlProtocolFixer.new(url: params[:url]).url

    if link = Link.where(url: url).first_or_create
      render json: link
    else
      render status: :bad_request
    end
  end

  def show
    link = Link.find_by_short_code(params[:short_code])
    if link
      redirect_to link.url
    else
      render status: :bad_request
    end
  end

end
