class Link < ApplicationRecord
  before_validation :valid_url?
  before_validation :assign_short_code

  validates :url, :short_code, uniqueness: true, presence: true

  def find_or_create_by_url(url)
    find_by(url: url) || create(url: url)
  end

  def short_url
    "/#{short_code}"
  end

  private

  def valid_url?
    "http://#{url}" unless url.blank? || url.include?('http')
  end

  def assign_short_code
    self.short_code = generate_code
    self.short_code = generate_code while Link.exists?(short_code: short_code)
  end

  def generate_code
    SecureRandom.hex(5)
  end

end
