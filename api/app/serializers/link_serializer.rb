class LinkSerializer < ActiveModel::Serializer
  attributes :short_url, :short_code, :url
end
